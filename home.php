<?php
session_start();
include_once 'database.php';

if(!isset($_SESSION['user']))
{
 header("Location: index.php");
}
$res=mysql_query("SELECT * FROM user_data WHERE uid=".$_SESSION['user']);
$userRow=mysql_fetch_array($res);
$userRow1=null;
$res1=mysql_query("SELECT `id`, `request_id`, `requester`, `req_type`, `priority`, `category`, `comment`, `filename`, `date_created`,`status` FROM `file_requests` WHERE uid=".$_SESSION['user']." ORDER BY `date_created` DESC LIMIT 0,10");
//$userRow1=mysql_fetch_row($res1);
$p=mysql_num_rows($res1);
$x=$_SESSION['user'];
$res2=mysql_query("SELECT  `pop`, `time`,`cancel_reqid` FROM `activites` where uid=$x ORDER BY `time` DESC");
$q=mysql_num_rows($res2);
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>User HomePage</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		<script language="javascript" src="js/jquery-1.2.6.min.js"></script>
		<script type="text/javascript" src="js/jquery-1.3.2.js"></script>
		<script type="text/javascript" src="js/jquery-1.11.3-jquery.min.js"></script>
		<script type="text/javascript" src="js/popup.js"></script>

    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">User Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $userRow['fname']; ?><i class="caret"></i></a>
								
                                <ul class="dropdown-menu">
                                    <li>
                                        <center><img src="<?php echo $userRow['avtarpath']; ?>" width="120px" height="120px" class="avatar img-circle img-thumbnail" alt="avatar" /></center>
                                    </li>
									<li class="divider"></li>
									<li>
                                        <a tabindex="-1" href="#" class="launchLink">Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
									<script language="javascript">
									$(function(){
										$('a#logout').click(function(){
											if(confirm('Are you sure you want to logout')) {
												return true;
											}

											return false;
										});
									});
									</script>
                                        <a tabindex="-1" id="logout" href="logout.php?logout">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
						<!--Experiment-->
						<div class="bgCover">&nbsp;</div>
						<div class="overlayBox">
							<div class="overlayContent">
								<a href="#" class="closeLink">[close]&times;</a>
								<!--<h2>Hello World</h2>
								<p>Feel free to put in what ever content you wish to. Images can also be shown</p>-->
								 <object type="text/html" data="profile.php" width="800px" height="550px" style="overflow:auto;">
    </object>
							</div>
						</div>
						
<script language="javascript">
function showOverlayBox() {
	//if box is not set to open then don't do anything
	if( isOpen == false ) return;
	// set the properties of the overlay box, the left and top positions
	$('.overlayBox').css({
		display:'block',
		left:( $(window).width() - $('.overlayBox').width() )/2,
		top:( $(window).height() - $('.overlayBox').height() )/2 -20,
		position:'absolute'
	});
	// set the window background for the overlay. i.e the body becomes darker
	$('.bgCover').css({
		display:'block',
		width: $(window).width(),
		height:$(window).height(),
	});
}
function doOverlayOpen() {
	//set status to open
	isOpen = true;
	showOverlayBox();
	$('.bgCover').css({opacity:0}).animate( {opacity:0.5, backgroundColor:'#000'} );
	// dont follow the link : so return false.
	return false;
}
function doOverlayClose() {
	//set status to closed
	isOpen = false;
	$('.overlayBox').css( 'display', 'none' );
	// now animate the background to fade out to opacity 0
	// and then hide it after the animation is complete.
	$('.bgCover').animate( {opacity:0}, null, null, function() { $(this).hide(); } );
}
// if window is resized then reposition the overlay box
$(window).bind('resize',showOverlayBox);
// activate when the link with class launchLink is clicked
$('a.launchLink').click( doOverlayOpen );
// close it when closeLink is clicked
$('a.closeLink').click( doOverlayClose );
	

</script>
						
						<!--Experiment-->
                        <ul class="nav">
                            <li class="active">
                                <a href="#">Dashboard</a>
                            </li>
                            
                            
                        </ul>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="#search">Search</a></li>
						</ul>
                    </div>
                    <!--/.nav-collapse -->
					<!--Search code-->
					
					<div id="search">
						<button type="button" class="close">X</button>
						<form id="searchform" method="post">
							<input type="search" id="searchbox" name="searchbox" value="" placeholder="type Request id here" autocomplete="off" required/>
							<button type="submit" id="reg-form" class="btn btn-primary">Search</button>
							
						</form>
					</div>
					
					<script language="javascript">
					$(function () {
						$('a[href="#search"]').on('click', function(event) {
							event.preventDefault();
							$('#search').addClass('open');
							$('#search > form > input[type="search"]').focus();
						});
						
						$('#search, #search button.close').on('click keyup', function(event) {
							if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
								$(this).removeClass('open');
							}
						});
						
						
						//Do not include! This prevents the form from submitting for DEMO purposes only!
						$('#searchform').submit(function(event) {
							var data = $(this).serialize();
							//event.preventDefault();
							$.ajax({
							type: 'POST',
							url: 'search.php',
							data: data,
							success: function(data){
									$(".search_res").html(data);
									
								//$(".search_res").load('search.php'); 
							}
							});
							return false;
						})
						
					});
					</script>
					<!--End search-->
                </div>
            </div>
        </div>
<script>
	function callme()
	{
		var e = document.getElementById("requestid");
		var reqid = e.options[e.selectedIndex].value;
      
		if(reqid!=0)
		{
			var sdata="func=showRequestprogress&request=" + reqid;
           
			$.ajax({ 
							type: "POST", 
							url: "home.php", 
							data: sdata,  
							success: function(msg1){
											
											$("#per").html(msg1);
											printValue('slider2','progress');
										
											
							}
				   });

		}
		else
		{
			$("#per").html("&nbsp;");
		}

	}
</script>
<?php
function showRequestprogress($request){
	$progressqry=mysql_query("select * from `progress` where `req_id`=$request");
	$progressrs=mysql_fetch_array($progressqry);
	$v= $progressrs['ppercent'];
	$b="<script language=\"javascript\"> $(\".bar\").animate({width: \"$v%\"}, 100 );";
	$b.="$(\"#slider2\").val($v); $(\"#slider2\").slider(\"refresh\");";
	//$b.="printValue('slider2','progress');";
	$b.="</script>";
	$b.="$v%";
	echo $b;
	
}
if(isset($_POST['func']))
	{
		$_POST['func']($_POST['request']);
	}
	else
	{	
?>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="settings.php"><i class="icon-chevron-right"></i>Settings</a>
                        </li>
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Welcome</h4>
                        	<sub><?php echo $userRow['email']; ?></sub></div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
										<label>
											<select name="requestid" id="requestid" onChange="callme();">
											<option value='0'>--Select Request ID--</option>
											<?php 
											$qry=mysql_query("select `request_id` from `file_requests` where `uid`=$x");
											while($rs = mysql_fetch_row($qry))
											{
													  echo "<option value='$rs[0]'>$rs[0]</option>";
											}
											?>
											</select>
										  </label>
										</li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>
                    
					<div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Progress of request</div>
                                
                            </div>
                            <div class="block-content collapse in">
                                
								<div class="progress progress-striped progress-success active">
											<div style="width: 0%;" class="bar"></div>
										</div><div id="per"></div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Request</div>
                                    <div class="pull-right">

                                    </div>
                                </div>
                                <div class="block-content collapse in"></br></br>
								<center>
								<a href="#" class="activator" id="activator"><button type="button" class="btn btn-success">Upload new request</button></a></br></br></br>
								<a href="#" class="activator1" id="activator1"><button type="button" class="btn btn-danger">Cancel old request</button></a></center></br></br>
                                </div>
                            </div>
                            <!-- /block -->
							<!--Request pop-->
							<div class="overlay" id="overlay" style="display:none;"></div>

							<div class="box" id="box">
							 <a class="boxclose" id="boxclose"></a>
							 <h3>New Upload Request</h3>
							 <!--<p>
							  Here comes a very important message for your user.
							  Turn this window off by clicking the cross.
							 </p>-->
							 <form id="requestform" class="formreq" enctype="multipart/form-data" method="post" action="newreq.php">
									
									<ul>
									
											<li>
								<label class="description" for="element_1">Request ID </label>
								<div>
									<input name="reqid" type="text" class="input-xlarge uneditable-input" maxlength="255" value="<?php $RndNumber = rand(1, 9999999999); echo $RndNumber;   ?>" style="height: 33.818182px;width: 201.818182px;line-height:30px;" required/> 
								</div> 
								</li>		<li>
								<label class="description" for="element_2">Requested By </label>
								<div>
									<input id="element_2" name="equester" class="element text medium" type="text" maxlength="255" value="<?php echo $userRow['fname']; ?>" style="height: 33.818182px;width: 201.818182px;line-height:30px;" disabled/> 
									<input type="hidden" name="requester" value="<?php echo $userRow['fname']; ?>">
								</div> 
								</li>			<li>
								<label class="description">What is type of request? </label>
								<ul id="radios">
									<li><input id="" name="type" class="type" type="radio" required value="Fare" />  Fare </li>
									<li><input id="" name="type" class="type" type="radio" required value="Rates" /> Rates </li>
									<li><input id="" name="type" class="type" type="radio" required value="Footnotes" /> Footnotes </li>
								</ul>
								 
								</li>		<li>
								<label class="description" for="element_9">What is request's priority? </label>
								<ul id="radios">
									<li><input id="" name="priority" class="priority" type="radio" required value="High" />  High </li>
									<li><input id="" name="priority" class="priority" type="radio" required value="Medium" /> Medium </li>
									<li><input id="" name="priority" class="priority" type="radio" required value="Low" /> Low </li>
								</ul> 
								</li>		<li>
								<label class="description" for="element_8">What is request's Category? </label>
								<ul id="radios">
									<li><input id="" name="category" class="category" type="radio" required value="Private" />  Private </li>
									<li><input id="" name="category" class="category" type="radio" required value="Public" /> Public </li>
									<li><input id="" name="category" class="category" type="radio" required value="Optional" /> Optional </li>
								</ul> 
								</li>		<li>
								<label class="description" for="element_6">Your comment to Supervisor </label>
								<div>
									<textarea id="element_6" name="msg" class="element textarea small"></textarea> 
								</div> 
								</li>		<li>
								<div class="control-group">
                                          <label class="control-label" for="fileInput">File input</label>
                                          <div class="controls">
                                            <input class="input-file uniform_on" id="fileInput" name="_file" type="file" required>
                                          </div>
                                        </div>  
								</li>
									
											<li>
										
										
										<input id="saveForm" class="button_text" type="submit" name="reqsubmit" value="Submit" />
								</li>
									</ul>
								</form>
							</div>
							<div class="overlay1" id="overlay1" style="display:none;"></div>

							<div class="box1" id="box1">
							 <a class="boxclose1" id="boxclose1"></a>
							 <h3>Cancel Old Request</h3>
							 <!--<p>
							  Here comes a very important message for your user.
							  Turn this window off by clicking the cross.
							 </p>-->
							 <script language="javascript">
					$(function () {
						$('a[href="#cancel"]').on('click', function(event) {
							event.preventDefault();
							$('#cancel').addClass('open');
							$('#cancel > form > input[type="search"]').focus();
						});
						
						
						
						
						//Do not include! This prevents the form from submitting for DEMO purposes only!
						$('#reqform').submit(function(event) {
							var data = $(this).serialize();
							//event.preventDefault();
							$.ajax({
							type: 'POST',
							url: 'cancel.php',
							data: data,
							success: function(data){
									$(".reqres").html(data);
									
								//$(".reqres").load('search.php'); 
							}
							});
							return false;
						})
						
					});
					</script>
					</br></br>
					<div id="cancel">
						
						<form id="reqform" method="post">
							<input type="search" id="reqbox" name="reqbox" value="" style="height: 33.818182px;width: 201.818182px;line-height:30px; " required/>
							<button type="submit" id="c_req" class="btn btn-primary">populate</button>
							<div class="reqres"></div>
						</form>
					</div>
							 
							</div>
							<!--end Request pop-->
                        </div>
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Notifications</div>
									<div class="pull-right"><span class="badge badge-info"><?php echo $q;?></span></div>
                                </div>
                                <div class="block-content collapse in" style="overflow:scroll; height:200px;" >
                                    
                                 <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>log</th>
                                                <th>created on</th>
                                                <th>on request ID</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
										
																				
										while($row1 = mysql_fetch_row($res2)){
											echo "<tr>";
											echo "<td>".$row1[0]."</td>";
											echo "<td>".$row1[1]."</td>";
											echo "<td>".$row1[2]."</td>";
											echo "</tr>";
										}
										?>
                                        </tbody>
                                    </table>
                                   
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Search Results</div>
                                    
                                </div>
                                <div class="block-content collapse in">
                                    <div class="search_res">Search Results Shown here</div>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Recently Uploaded files</div>
									<div class="pull-right"><span class="badge badge-info"><?php echo $p;?></span></div>
                                </div>
                                <div class="block-content collapse in" style="overflow:scroll; height:200px;">
								
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Req ID</th>
                                                <th>created on</th>
                                                <th>Filename</th>
                                            </tr>
                                        </thead>
										<tbody>
										<?php
										
																				
										while($row = mysql_fetch_row($res1)){
											echo "<tr>";
											echo "<td>".$row[1]."</td>";
											echo "<td>".$row[8]."</td>";
											echo "<td>".$row[7]."</td>";
											echo "</tr>";
										}
										?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    </div>
                    <div class="row-fluid">
                        <!-- block -->
                        
                        <!-- /block -->
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Anuj Shah</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
        </script>
	<?php }?>
    </body>

</html>