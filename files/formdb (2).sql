-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2016 at 06:16 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `formdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `formdata`
--

CREATE TABLE IF NOT EXISTS `formdata` (
  `userassoc` varchar(30) NOT NULL,
  `brandnm` varchar(50) NOT NULL,
  `companynm` varchar(50) NOT NULL,
  `contact1` varchar(50) NOT NULL,
  `mobile1` varchar(50) NOT NULL,
  `contact2` varchar(50) NOT NULL,
  `mobile2` varchar(50) NOT NULL,
  `address` varchar(120) NOT NULL,
  `office1` varchar(20) NOT NULL,
  `office2` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `website` varchar(100) NOT NULL,
  `products` varchar(100) NOT NULL,
  `category` varchar(30) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `profile` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `formdata`
--

INSERT INTO `formdata` (`userassoc`, `brandnm`, `companynm`, `contact1`, `mobile1`, `contact2`, `mobile2`, `address`, `office1`, `office2`, `email`, `website`, `products`, `category`, `logo`, `profile`) VALUES
('anuj', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abcabcabcabc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,Ladies,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/back.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,Boys,Girls,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,Boys,Girls,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,Boys,Girls,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,Boys,Girls,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,Boys,Girls,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,Boys,Girls,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,Boys,Girls,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,Boys,Girls,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,Boys,Girls,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,Boys,Girls,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abcd', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,Boys,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' ', 'http://localhost/login/files/', 'http://localhost/login/files/'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' ', 'http://localhost/login/files/', 'http://localhost/login/files/'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' ', 'http://localhost/login/files/', 'http://localhost/login/files/'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,', 'http://localhost/login/files/Capture.PNG', 'http://localhost/login/files/Capture.PNG'),
('Anuj', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abcabcabcabc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,', 'http://localhost/login/files/unnamed.png', 'http://localhost/login/files/unnamed.png'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,', 'http://localhost/login/files/Capture.PNG', 'http://localhost/login/files/Capture.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG'),
('anujshah', 'abc', 'abc', 'abc', '123', 'abc', '123', 'abc', '123', '123', 'anuj.shah70@yahoo.com', 'abc.com', 'abc', ' Mens,', 'http://localhost/login/files/2.PNG', 'http://localhost/login/files/2.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`) VALUES
(7, 'anuj', '1234', 'anuj.shah70@yahoo.com'),
(8, 'anujshah', '1234', 'anuj.shah70@yahoo.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
