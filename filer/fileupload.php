<?php
   if(isset($_FILES['_file'])){
      $errors= array();
      $file_name = $_FILES['_file']['name'];
      $file_size = $_FILES['_file']['size'];
      $file_tmp = $_FILES['_file']['tmp_name'];
      $file_type = $_FILES['_file']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['_file']['name'])));
      
      $expensions= array("jpeg","jpg","png","pdf","docx","xlsx");
      
      if(in_array($file_ext,$expensions)=== false){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
      
      if($file_size > 20971520) {
         $errors[]='File size must be excately 20 MB';
      }
      
      if(empty($errors)==true) {
         move_uploaded_file($file_tmp,"files/".time().$file_name);
         echo "Success";
      }else{
         print_r($errors);
      }
   }
?>
<html>
   <body>
      
      <form action = "" method = "POST" enctype = "multipart/form-data">
         <input type = "file" name = "_file" />
         <input type = "submit"/>
			
         <ul>
            <li>Sent file: <?php if(isset($_FILES['_file']['name']))echo $_FILES['_file']['name'];  ?>
            <li>File size: <?php if(isset($_FILES['_file']['size']))echo $_FILES['_file']['size'];  ?>
            <li>File type: <?php if(isset($_FILES['_file']['type']))echo $_FILES['_file']['type'];	 ?>
         </ul>
			
      </form>
      
   </body>
</html>